/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablas;

import prograproyecto.Genero;
import prograproyecto.Pais;
import prograproyecto.Redsocial;
import prograproyecto.Usuario;

/**
 * Clase persona para ser usada y transformada a distintos formatos.
 * @author catalinaaguayo
 */
public class PersonaTabla {

    private String primerNombre;
    private String segundoNombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private int edad;
    private prograproyecto.Usuario usuario_idUsuario;

    public static PersonaTabla createPersona(String primerNombre, String segundoNombre, String apellidoPaterno, String apellidoMaterno, Integer edad, Usuario usuario_idUsuario) {
        return new PersonaTabla(primerNombre, segundoNombre,  apellidoPaterno, apellidoMaterno, edad, usuario_idUsuario);
    }

    public PersonaTabla(String primerNombre, String segundoNombre, String apellidoPaterno, String apellidoMaterno, Integer edad, Usuario usuario_idUsuario) {
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.edad = edad;
        this.usuario_idUsuario = usuario_idUsuario;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Usuario getUsuario_idUsuario() {
        return usuario_idUsuario;
    }

    public void setUsuario_idUsuario(Usuario usuario_idUsuario) {
        this.usuario_idUsuario = usuario_idUsuario;
    }

    
}
