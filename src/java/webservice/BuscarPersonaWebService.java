/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import prograproyecto.Persona;

/**
 *
 * @author catalinaaguayo
 */
@WebServlet(name = "PersonaWebService", urlPatterns = {"/PersonaWebService"})
public class BuscarPersonaWebService extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        // Url = http://localhost:8080/PrograProyecto5/BuscarPersonaWebService?primerNombre='NOMBRE'
        
        String primerNombre = request.getParameter("primerNombre");
        String resultado="";

        if ( primerNombre != null) {
            ormsamples.BuscarData buscardata = new ormsamples.BuscarData();
            Persona persona = buscardata.searchPersona(primerNombre);
            if (persona!=null) {
                resultado = mostrarPersona(persona);
            }
            else {
                resultado = "Persona no encontrada";
            }
        }

        PrintWriter out = response.getWriter();
        out.println(resultado);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String mostrarPersona(Persona persona){
        return "Persona: {PrimerNombre:"+persona.getPrimerNombre()+","
                + "SegundoNombre"+persona.getSegundoNombre()+","
                + "ApellidoPaterno"+persona.getApellidoPaterno()+","
                + "ApellidoMaterno"+persona.getApellidoMaterno()+"}";
    }
    
}
