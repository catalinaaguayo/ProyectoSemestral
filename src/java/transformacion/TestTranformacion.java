/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transformacion;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Clase que genera trasnformaciones desde xml a distintos formatos
 *
 * @author catalinaaguayo
 */
public class TestTranformacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        transformarAExcel();
        
    }

    /**
     * Método que realiza transformación de un archivo xml a otro formato
     *
     * @param xslDocS archivo xsl que define el estilo de la transformación
     * @param xmlDocS archivo xml sobre el cual se hace la transformación
     * @param outputDocS archivo que genera la transformación
     * @throws FileNotFoundException
     * @throws TransformerException
     */
    public static void transformar(String xslDocS, String xmlDocS, String outputDocS) throws FileNotFoundException, TransformerException {
        //Se crea transformer factory el cual creara el objeto transformer
        TransformerFactory tFactory = TransformerFactory.newInstance();
        //Fuente del archivo xsl
        Source xslDoc = new StreamSource(xslDocS);
        //Fuente archivo xml
        Source xmlDoc = new StreamSource(xmlDocS);
        //Path del archivo de salida
        String outputFileName = outputDocS;
        //Se crea OutputStream con direccion al path de archivo de salida
        OutputStream htmlFile = new FileOutputStream(outputFileName);
        //Se crea el transformer respecto al archivo xsl
        Transformer trasform = tFactory.newTransformer(xslDoc);
        //Se transforma el documento xsl y se envia al documento de salida
        trasform.transform(xmlDoc, new StreamResult(htmlFile));
    }

    /**
     * Método que genera archivo html a partir de xml.
     *
     * @throws FileNotFoundException
     */
    public static void transformarAHtml() throws FileNotFoundException {
        try {
            System.out.println("Realizando transformación a html...");
            transformar("/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/transformacion/RegistroPersonasHtml.xsl",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.xml",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.html");
        } catch (TransformerException ex) {
            Logger.getLogger(TestTranformacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que genera archivo excel a partir de xml.
     * @throws FileNotFoundException
     */
    public static void transformarAExcel() throws FileNotFoundException {
        try {
            System.out.println("Realizando transformación a excel...");
            transformar("/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/transformacion/RegistroPersonasExcel.xsl",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.xml",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.xls");
        } catch (TransformerException ex) {
            Logger.getLogger(TestTranformacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que genera archivo word a partir de xml.
     * @throws FileNotFoundException
     */
    public static void transformarAWord() throws FileNotFoundException {
        try {
            System.out.println("Realizando transformación a word...");
            transformar("/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/transformacion/RegistroPersonasWord.xsl",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.xml",
                    "/Users/catalinaaguayo/NetBeansProjects/PrograProyecto5/src/java/reportes/RegistroPersonas.doc");
        } catch (TransformerException ex) {
            Logger.getLogger(TestTranformacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
