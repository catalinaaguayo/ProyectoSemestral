<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>

            <body>
                <h2>Estudiantes Reprobando</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Primer Nombre</th>
                        <th>Segundo Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Edad</th>
                        <th>Usuario</th>
                        <th>Contrasena</th>
                    </tr>
                    <tr>
                        <xsl:for-each select="list/tablas.PersonaTabla">
                            <tr>
                                <td>
                                    <xsl:value-of select="primerNombre"/>
                                </td>
                                <td>
                                    <xsl:value-of select="segundoNombre"/>
                                </td>
                                <td>
                                    <xsl:value-of select="apellidoPaterno"/>
                                </td>
                                <td>
                                    <xsl:value-of select="apellidoMaterno"/>
                                </td>
                                <td>
                                    <xsl:value-of select="edad"/>
                                </td>
                                <td>
                                    <xsl:value-of select="usuario__idUsuario/usuario"/>
                                </td>
                                <td>
                                    <xsl:value-of select="usuario__idUsuario/contrasena"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
