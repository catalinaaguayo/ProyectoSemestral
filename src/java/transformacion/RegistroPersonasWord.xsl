<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
            <w:body>
                <xsl:for-each select="list/tablas.PersonaTabla">
                    <w:p>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="primerNombre"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="segundoNombre"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="apellidoPaterno"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="apellidoMaterno"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="edad"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="usuario__idUsuario/usuario"/>
                            </w:t>
                        </w:r>
                        <w:r>
                            <w:t>
                                <xsl:value-of select="usuario__idUsuario/contrasena"/>
                            </w:t>
                        </w:r>
            
                    </w:p>
                </xsl:for-each>
            </w:body>
        </w:wordDocument>
    </xsl:template>
</xsl:stylesheet>
