<xsl:stylesheet
    version="1.0"
    xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:x="urn:schemas-microsoft-com:office:excel"
    xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">

    <xsl:template match="/">
        <Workbook
            xmlns="urn:schemas-microsoft-com:office:spreadsheet"
            xmlns:o="urn:schemas-microsoft-com:office:office"
            xmlns:x="urn:schemas-microsoft-com:office:excel"
            xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
            xmlns:html="http://www.w3.org/TR/REC-html40">
            <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
                <Author>ed woychowsky</Author>
                <LastAuthor>Edmond Woychowsky</LastAuthor>
                <Created>2007-01-26T16:54:15Z</Created>
                <LastSaved>2007-01-27T05:18:54Z</LastSaved>
                <Company>None</Company>
                <Version>10.3501</Version>
            </DocumentProperties>
            <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
                <DownloadComponents/>
                <LocationOfComponents HRef="file:///D:\"/>
            </OfficeDocumentSettings>
            <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
                <WindowHeight>8700</WindowHeight>
                <WindowWidth>11355</WindowWidth>
                <WindowTopX>480</WindowTopX>
                <WindowTopY>120</WindowTopY>
                <ProtectStructure>False</ProtectStructure>
                <ProtectWindows>False</ProtectWindows>
            </ExcelWorkbook>
            <Styles>
                <Style ss:ID="Default" ss:Name="Normal">
                    <Alignment ss:Vertical="Bottom"/>
                    <Borders/>
                    <Font/>
                    <Interior/>
                    <NumberFormat/>
                    <Protection/>
                </Style>
            </Styles>
            <Worksheet ss:Name="Test">
                <Table>
                    <Row>
                        <Cell>
                            <Data ss:Type="String">Primer Nomnre</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Segundo Nomnre</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Apellido Paterno</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Apellido Materno</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Edad</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Usuario</Data>
                        </Cell>
                        <Cell>
                            <Data ss:Type="String">Contrasena</Data>
                        </Cell>
                    </Row>

                    <xsl:for-each select="list/tablas.PersonaTabla">
                        <Row>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="primerNombre"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="segundoNombre"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="apellidoPaterno"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="apellidoMaterno"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="edad"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="usuario__idUsuario/usuario"/>
                                </Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">
                                    <xsl:value-of select="usuario__idUsuario/contrasena"/>
                                </Data>
                            </Cell>
                            
                        </Row>
                    </xsl:for-each>
                </Table>
                <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                    <Print>
                        <ValidPrinterInfo/>
                        <HorizontalResolution>600</HorizontalResolution>
                        <VerticalResolution>0</VerticalResolution>
                    </Print>
                    <Selected/>
                    <Panes>
                        <Pane>
                            <Number>3</Number>
                            <ActiveRow>2</ActiveRow>
                        </Pane>
                    </Panes>
                    <ProtectObjects>False</ProtectObjects>
                    <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
            </Worksheet>
            <Worksheet ss:Name="Sheet2">
                <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                    <ProtectObjects>False</ProtectObjects>
                    <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
            </Worksheet>
            <Worksheet ss:Name="Sheet3">
                <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                    <ProtectObjects>False</ProtectObjects>
                    <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
            </Worksheet>
        </Workbook>
    </xsl:template>
</xsl:stylesheet>
