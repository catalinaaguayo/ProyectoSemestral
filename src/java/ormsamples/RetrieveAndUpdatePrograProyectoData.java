/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdatePrograProyectoData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = prograproyecto.PrograProyectoPersistentManager.instance().getSession().beginTransaction();
		try {
			prograproyecto.Persona lprograproyectoPersona = prograproyecto.PersonaDAO.loadPersonaByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.PersonaDAO.save(lprograproyectoPersona);
			prograproyecto.Pais lprograproyectoPais = prograproyecto.PaisDAO.loadPaisByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.PaisDAO.save(lprograproyectoPais);
			prograproyecto.Genero lprograproyectoGenero = prograproyecto.GeneroDAO.loadGeneroByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.GeneroDAO.save(lprograproyectoGenero);
			prograproyecto.Usuario lprograproyectoUsuario = prograproyecto.UsuarioDAO.loadUsuarioByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.UsuarioDAO.save(lprograproyectoUsuario);
			prograproyecto.Redsocial lprograproyectoRedsocial = prograproyecto.RedsocialDAO.loadRedsocialByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.RedsocialDAO.save(lprograproyectoRedsocial);
			prograproyecto.Proyecto lprograproyectoProyecto = prograproyecto.ProyectoDAO.loadProyectoByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.ProyectoDAO.save(lprograproyectoProyecto);
			prograproyecto.Tipoproyecto lprograproyectoTipoproyecto = prograproyecto.TipoproyectoDAO.loadTipoproyectoByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TipoproyectoDAO.save(lprograproyectoTipoproyecto);
			prograproyecto.Material lprograproyectoMaterial = prograproyecto.MaterialDAO.loadMaterialByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.MaterialDAO.save(lprograproyectoMaterial);
			prograproyecto.Tiendamaterial lprograproyectoTiendamaterial = prograproyecto.TiendamaterialDAO.loadTiendamaterialByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TiendamaterialDAO.save(lprograproyectoTiendamaterial);
			prograproyecto.Tiendausuario lprograproyectoTiendausuario = prograproyecto.TiendausuarioDAO.loadTiendausuarioByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TiendausuarioDAO.save(lprograproyectoTiendausuario);
			prograproyecto.Tutorial lprograproyectoTutorial = prograproyecto.TutorialDAO.loadTutorialByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TutorialDAO.save(lprograproyectoTutorial);
			prograproyecto.Tiponudo lprograproyectoTiponudo = prograproyecto.TiponudoDAO.loadTiponudoByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TiponudoDAO.save(lprograproyectoTiponudo);
			prograproyecto.Tipomaterial lprograproyectoTipomaterial = prograproyecto.TipomaterialDAO.loadTipomaterialByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TipomaterialDAO.save(lprograproyectoTipomaterial);
			prograproyecto.Tipohilo lprograproyectoTipohilo = prograproyecto.TipohiloDAO.loadTipohiloByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TipohiloDAO.save(lprograproyectoTipohilo);
			prograproyecto.Tipopatron lprograproyectoTipopatron = prograproyecto.TipopatronDAO.loadTipopatronByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.TipopatronDAO.save(lprograproyectoTipopatron);
			prograproyecto.Producto lprograproyectoProducto = prograproyecto.ProductoDAO.loadProductoByQuery(null, null);
			// Update the properties of the persistent object
			prograproyecto.ProductoDAO.save(lprograproyectoProducto);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdatePrograProyectoData retrieveAndUpdatePrograProyectoData = new RetrieveAndUpdatePrograProyectoData();
			try {
				retrieveAndUpdatePrograProyectoData.retrieveAndUpdateTestData();
			}
			finally {
				prograproyecto.PrograProyectoPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
