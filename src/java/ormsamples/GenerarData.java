/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormsamples;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Generación de elementos para el llenado de la base de datos
 *
 * @author catalinaaguayo
 */
public class GenerarData {

    /**
     * Método que obtiene la ruta de un archivo.
     *
     * @return Ruta del archivo
     */
    public static String getUrl() {
        return System.getProperty("user.dir");
    }

    /**
     * Genera un nombre aleatorio tomando uno del archivo nombres.txt
     *
     * @return Nombre aleatorio
     */
    public static String getRandomNombre() {
        ArrayList<String> Nombres = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/nombres.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    Nombres.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int fnRandom = ThreadLocalRandom.current().nextInt(0, Nombres.size());
        return Nombres.get(fnRandom);
    }

    /**
     * Genera un apellido aleatorio tomando uno del archivo apellidos.txt
     *
     * @return Apellido aleatorio
     */
    public static String getRandomApellido() {
        ArrayList<String> Apellidos = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/apellidos.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    Apellidos.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int lnRandom = ThreadLocalRandom.current().nextInt(0, Apellidos.size());
        return Apellidos.get(lnRandom);
    }

    /**
     * Genera un pais aleatorio tomando uno del archivo paises.txt
     *
     * @return Pais aleatorio
     */
    public static String getRandomPais() {
        ArrayList<String> Paises = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/paises.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    Paises.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int lnRandom = ThreadLocalRandom.current().nextInt(0, Paises.size());
        return Paises.get(lnRandom);
    }

    /**
     * Genera un tipo de proyecto aleatorio tomando uno del archivo
     * tiposproyecto.txt
     *
     * @return Tipo de proyecto aleatorio
     */
    public static String getRandomTipoProyecto() {
        ArrayList<String> TiposProyecto = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/tiposproyecto.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    TiposProyecto.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int lnRandom = ThreadLocalRandom.current().nextInt(0, TiposProyecto.size());
        return TiposProyecto.get(lnRandom);
    }

    /**
     * Genera un tipo de nudo aleatorio tomando uno del archivo tiposnudo.txt
     *
     * @return Tipo de nudo aleatorio
     */
    public static String getRandomTipoNudo() {
        ArrayList<String> TiposNudo = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/tiposnudo.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    TiposNudo.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int lnRandom = ThreadLocalRandom.current().nextInt(0, TiposNudo.size());
        return TiposNudo.get(lnRandom);
    }

    /**
     * Genera un género aleatorio tomando uno del archivo generos.txt
     *
     * @return Género aleatorio
     */
    public static String getRandomGenero() {
        ArrayList<String> Generos = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/generos.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    Generos.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int lnRandom = ThreadLocalRandom.current().nextInt(0, Generos.size());
        return Generos.get(lnRandom);
    }

    /**
     * Genera un tipo de patrón aleatorio tomando uno del archivo tipopatron.txt
     *
     * @return Tipo de patron aleatorio
     */
    public static String getRandomTipoPatron() {
        ArrayList<String> Generos = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(getUrl() + "/src/java/documentos/tipopatron.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    Generos.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int lnRandom = ThreadLocalRandom.current().nextInt(0, Generos.size());
        return Generos.get(lnRandom);
    }

    /**
     * Lee el contenido de un archivo y lo entrega como string
     *
     * @param filePath Ruta del archivo
     * @return El contenido del archivo
     */
    public static String readFile(String filePath) {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

            String line;

            while ((line = br.readLine()) != null) {
                builder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    /**
     * Genera un número aleatorio entre el rango del min y max
     *
     * @param min Mínimo del rango
     * @param max Máximo del rango
     * @return Número aleatorio
     */
    public static int getNumeroRandom(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

}
