/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormsamples;

import com.google.gson.Gson;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import tablas.PersonaTabla;

/**
 * Clase que genera un archivo Json a partir de los registros de la base de datos.
 * @author catalinaaguayo
 */
public class ToJson {

    /**
     * @param args the command line arguments
     */
    private static final int ROW_COUNT = 100;

    public static void main(String[] args) {
        generarJson();
    }

    /**
     * Crea registros en formato json.
     */
    public static void generarJson() {
        System.out.println("Generando archivo json...");
        Collection<PersonaTabla> ArregloPersonas = new ArrayList<PersonaTabla>();
        prograproyecto.Persona[] prograproyectoPersonas = null;
        try {
            prograproyectoPersonas = prograproyecto.PersonaDAO.listPersonaByQuery(null, null);
        } catch (PersistentException ex) {
            Logger.getLogger(ToJson.class.getName()).log(Level.SEVERE, null, ex);
        }
        int length = Math.min(prograproyectoPersonas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
                        ArregloPersonas.add(PersonaTabla.createPersona(
                                prograproyectoPersonas[i].getPrimerNombre(), 
                                prograproyectoPersonas[i].getSegundoNombre(),
                                prograproyectoPersonas[i].getApellidoPaterno(),
                                prograproyectoPersonas[i].getApellidoMaterno(),
                                prograproyectoPersonas[i].getEdad(),  
                                prograproyectoPersonas[i].getUsuario_idUsuario()));
        }
        System.out.println("hola");
        String json = new Gson().toJson(ArregloPersonas);
        System.out.println("chao");
        System.out.println("JSON->" + json);  
        generarArchivoJson(json);
    }
    
        /**
         * Crea archivo json 
         * @param datos un string que contiene los datos a guardar.
         */
        public static void generarArchivoJson(String datos) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        String ruta = "src/java/reportes/RegistroPersonas.json";
        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);

            pw.println(datos);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        }

}
