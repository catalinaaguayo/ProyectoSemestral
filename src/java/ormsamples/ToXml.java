/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormsamples;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import tablas.PersonaTabla;

/**
 * Clase que genera un archivo Xml a partir de los registros de la base de
 * datos.
 *
 * @author catalinaaguayo
 */
public class ToXml {

    /**
     * @param args the command line arguments
     */
    private static final int ROW_COUNT = 100;

    public static void main(String[] args) {
        generarXml();
    }

    /**
     * Crea registros en formato xml
     */
    public static void generarXml() {
        System.out.println("Generando archivo xml...");
        Collection<PersonaTabla> ArregloPersonas = new ArrayList<PersonaTabla>();
        prograproyecto.Persona[] prograproyectoPersonas = null;
        try {
            prograproyectoPersonas = prograproyecto.PersonaDAO.listPersonaByQuery(null, null);
        } catch (PersistentException ex) {
            Logger.getLogger(ToXml.class.getName()).log(Level.SEVERE, null, ex);
        }
        int length = Math.min(prograproyectoPersonas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
                        ArregloPersonas.add(PersonaTabla.createPersona(
                                prograproyectoPersonas[i].getPrimerNombre(), 
                                prograproyectoPersonas[i].getSegundoNombre(),
                                prograproyectoPersonas[i].getApellidoPaterno(),
                                prograproyectoPersonas[i].getApellidoMaterno(),
                                prograproyectoPersonas[i].getEdad(),  
                                prograproyectoPersonas[i].getUsuario_idUsuario()));

        }
        //String xml = new XStream(new DomDriver()).toXML(ArregloPersonas);
        XStream xstream = new XStream(new DomDriver());
        xstream.ignoreUnknownElements();
        String xml = xstream.toXML(ArregloPersonas);
        generarArchivoXML(xml);
    }

    /**
     * Crea archivo xml
     *
     * @param datos un string que contiene los datos a guardar.
     */
    private static void generarArchivoXML(String datos) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        String ruta = "src/java/reportes/RegistroPersonas.xml";
        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);
            pw.println(datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

}
