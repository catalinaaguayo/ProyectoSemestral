/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
import prograproyecto.*;

/**
 * Clase que maneja toda la creación de los datos con métodos por separado.
 *
 * @author catalinaaguayo
 */
public class CrearData {

    public CrearData() {
    }

    /**
     * Crea persona con sus respectivos atributos que se le cargan.
     *
     * @param primerNombre
     * @param segundoNombre
     * @param apellidoMaterno
     * @param apellidoPaterno
     * @param edad
     * @param correo
     * @param nombreGenero
     * @param nombrePais
     * @param nombreFacebook
     * @param nombreYoutube
     * @param nombreInstagram
     * @param nombreTwitter
     * @throws PersistentException
     */
    public void createTestData(String primerNombre, String segundoNombre,
            String apellidoMaterno, String apellidoPaterno, int edad,
            String correo, String nombreGenero, String nombrePais,
            String nombreFacebook, String nombreYoutube, String nombreInstagram, String nombreTwitter) throws PersistentException {
        PersistentTransaction t = prograproyecto.PrograProyectoPersistentManager.instance().getSession().beginTransaction();
        try {
            crearPersonaNueva(primerNombre, segundoNombre,
                    apellidoMaterno, apellidoPaterno, edad,
                    correo, nombreGenero, nombrePais,
                    nombreFacebook, nombreYoutube, nombreInstagram, nombreTwitter);
            t.commit();
        } catch (PersistentException e) {
            t.rollback();
        }
    }

    /**
     * Método que genera un registro (para el llenado).
     *
     * @param primerNombre
     */
    public void correr(String primerNombre, String segundoNombre,
            String apellidoMaterno, String apellidoPaterno, int edad,
            String correo, String nombreGenero, String nombrePais,
            String nombreFacebook, String nombreYoutube, String nombreInstagram, String nombreTwitter) {
        try {
            CrearData createPrograProyectoData = new CrearData();
            try {
                createPrograProyectoData.createTestData(primerNombre, segundoNombre,
                        apellidoMaterno, apellidoPaterno, edad,
                        correo, nombreGenero, nombrePais,
                        nombreFacebook, nombreYoutube, nombreInstagram, nombreTwitter);
            } finally {
                prograproyecto.PrograProyectoPersistentManager.instance().disposePersistentManager();
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea persona utilizando métodos que generan datos aleatorios y la guarda
     * en la base de datos
     *
     * @throws PersistentException
     */
    public static void crearPersona() throws PersistentException {
        prograproyecto.Persona Persona = prograproyecto.PersonaDAO.createPersona();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : usuario, genero_idGenero, redSocial_idRedSocial, pais_idPais

        //Establece atributos de persona
        Persona.setApellidoMaterno(GenerarData.getRandomApellido());
        Persona.setApellidoPaterno(GenerarData.getRandomApellido());
        Persona.setPrimerNombre(GenerarData.getRandomNombre());
        Persona.setSegundoNombre(GenerarData.getRandomNombre());
        Persona.setEdad(GenerarData.getNumeroRandom(5, 99));

        //Crea país de persona
        //Pais pais = crearPais();
        //Persona.setPais_idPais(pais);
        //Crea género de persona
        //Genero genero = crearGenero();
        //Persona.setGenero_idGenero(genero);
        //Crea redes sociales de la persona
        //Redsocial redsocial = crearRedSocial(Persona);
        //Persona.setRedSocial_idRedSocial(redsocial);
        //Crea usuario de la persona
        //Usuario usuario = crearUsuario(Persona);
        //Persona.setUsuario_idUsuario(usuario);
        //Guarda la persona creada
        prograproyecto.PersonaDAO.save(Persona);
    }

    public void crearPersonaNueva(String primerNombre, String segundoNombre,
            String apellidoMaterno, String apellidoPaterno, int edad,
            String correo, String nombreGenero, String nombrePais,
            String nombreFacebook, String nombreYoutube, String nombreInstagram, String nombreTwitter) throws PersistentException {
        prograproyecto.Persona Persona = prograproyecto.PersonaDAO.createPersona();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : usuario, genero_idGenero, redSocial_idRedSocial, pais_idPais

        //Establece atributos de persona
        Persona.setApellidoMaterno(apellidoMaterno);
        Persona.setApellidoPaterno(apellidoPaterno);
        Persona.setPrimerNombre(primerNombre);
        Persona.setSegundoNombre(segundoNombre);
        Persona.setEdad(edad);

        //Crea país de persona
        Pais pais = crearPais(nombrePais);
        Persona.setPais_idPais(pais);

        //Crea género de persona
        Genero genero = crearGenero(nombreGenero);
        Persona.setGenero_idGenero(genero);

        //Crea redes sociales de la persona
        Redsocial redsocial = crearRedSocial(nombreFacebook, nombreInstagram,
                nombreTwitter, nombreYoutube);
        Persona.setRedSocial_idRedSocial(redsocial);

        //Crea usuario de la persona
        Usuario usuario = crearUsuario(Persona);
        Persona.setUsuario_idUsuario(usuario);

        //Guarda la persona creada
        prograproyecto.PersonaDAO.save(Persona);
    }

    public static Pais crearPais(String nombrePais) throws PersistentException {
        prograproyecto.Pais Pais = prograproyecto.PaisDAO.createPais();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tiendaMaterial, persona
        Pais.setPais(nombrePais);
        prograproyecto.PaisDAO.save(Pais);
        return Pais;
    }

    public static Genero crearGenero(String nombreGenero) throws PersistentException {
        prograproyecto.Genero Genero = prograproyecto.GeneroDAO.createGenero();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : persona
        //ARREGLAR
        Genero.setGenero(nombreGenero);
        prograproyecto.GeneroDAO.save(Genero);
        return Genero;
    }

    public Usuario crearUsuario(Persona persona) throws PersistentException {
        prograproyecto.Usuario Usuario = prograproyecto.UsuarioDAO.createUsuario();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tiendaUsuario, proyecto, persona_idPersona, usuario
        Usuario.setUsuario(persona.getApellidoPaterno() + persona.getPrimerNombre());
        Usuario.setContrasena("1234567");
        prograproyecto.UsuarioDAO.save(Usuario);
        return Usuario;
    }

    public static Redsocial crearRedSocial(String nombreFacebook, String nombreInstagram,
            String nombreYoutube, String nombreTwitter) throws PersistentException {
        prograproyecto.Redsocial RedSocial = prograproyecto.RedsocialDAO.createRedsocial();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : persona
        RedSocial.setUsuarioFacebook(nombreFacebook);
        RedSocial.setUsuarioInstagram(nombreInstagram);
        RedSocial.setUsuarioTwitter(nombreTwitter);
        RedSocial.setUsuarioYoutube(nombreYoutube);
        prograproyecto.RedsocialDAO.save(RedSocial);
        return RedSocial;
    }

    public Proyecto crearProyecto(String nombreproyecto) throws PersistentException {
        prograproyecto.Proyecto Proyecto = prograproyecto.ProyectoDAO.createProyecto();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tipoNudo, material, tutorial, tipoPatron, producto_idProducto, tipoProyecto_idTipo, usuario_idUsuario
        Proyecto.setNombre(nombreproyecto);
        prograproyecto.ProyectoDAO.save(Proyecto);
        return Proyecto;
    }

    public Tipoproyecto crearTipoProyecto(String tipoproyecto, Proyecto proyecto) throws PersistentException {
        prograproyecto.Tipoproyecto TipoProyecto = prograproyecto.TipoproyectoDAO.createTipoproyecto();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyecto
        TipoProyecto.setTipo(tipoproyecto);
        TipoProyecto.setProyecto(proyecto);
        prograproyecto.TipoproyectoDAO.save(TipoProyecto);
        return TipoProyecto;
    }

    public Material crearMaterial(String nombrematerial, Proyecto proyecto, String descripcion) throws PersistentException {
        prograproyecto.Material Material = prograproyecto.MaterialDAO.createMaterial();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyectoidProyecto, tipoHiloidTipoHilo, tipoMaterialidTipoMaterial, tiendaMaterial_idTiendaMaterial
        Material.setNombreMaterial(nombrematerial);
        Material.setProyectoidProyecto(proyecto);
        Material.setDescripcionMaterial(descripcion);
        prograproyecto.MaterialDAO.save(Material);
        return Material;
    }

    public Tiendamaterial crearTiendaMaterial() throws PersistentException {
        prograproyecto.Tiendamaterial TiendaMaterial = prograproyecto.TiendamaterialDAO.createTiendamaterial();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : material, pais_idPais
        prograproyecto.TiendamaterialDAO.save(TiendaMaterial);
        return TiendaMaterial;
    }

    public Tiendausuario crearTiendaUsuario() throws PersistentException {
        prograproyecto.Tiendausuario TiendaUsuario = prograproyecto.TiendausuarioDAO.createTiendausuario();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : usuario_idUsuario
        prograproyecto.TiendausuarioDAO.save(TiendaUsuario);
        return TiendaUsuario;
    }

    public Tutorial crearTutorial() throws PersistentException {
        prograproyecto.Tutorial Tutorial = prograproyecto.TutorialDAO.createTutorial();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyectoidProyecto
        prograproyecto.TutorialDAO.save(Tutorial);
        return Tutorial;
    }

    public Tiponudo crearTipoNudo() throws PersistentException {
        prograproyecto.Tiponudo TipoNudo = prograproyecto.TiponudoDAO.createTiponudo();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyectoidProyecto
        TipoNudo.setNudo(GenerarData.getRandomTipoNudo());
        prograproyecto.TiponudoDAO.save(TipoNudo);
        return TipoNudo;
    }

    public Tipomaterial crearTipoMaterial() throws PersistentException {
        prograproyecto.Tipomaterial TipoMaterial = prograproyecto.TipomaterialDAO.createTipomaterial();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : material
        prograproyecto.TipomaterialDAO.save(TipoMaterial);
        return TipoMaterial;
    }

    public Tipohilo crearTipoHilo() throws PersistentException {
        prograproyecto.Tipohilo TipoHilo = prograproyecto.TipohiloDAO.createTipohilo();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : material
        prograproyecto.TipohiloDAO.save(TipoHilo);
        return TipoHilo;
    }

    public Tipopatron crearTipoPatron(String nombrePatron) throws PersistentException {
        prograproyecto.Tipopatron TipoPatron = prograproyecto.TipopatronDAO.createTipopatron();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyectoidProyecto
        TipoPatron.setTipoPatron(nombrePatron);
        prograproyecto.TipopatronDAO.save(TipoPatron);
        return TipoPatron;
    }

    public Producto crearProducto() throws PersistentException {
        prograproyecto.Producto Producto = prograproyecto.ProductoDAO.createProducto();
        // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : proyecto, proyectoidProyecto
        prograproyecto.ProductoDAO.save(Producto);
        return Producto;
    }

}
