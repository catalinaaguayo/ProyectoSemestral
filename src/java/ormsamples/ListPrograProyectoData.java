/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.Collection;
import org.orm.*;
import prograproyecto.Persona;
import tablas.PersonaTabla;
public class ListPrograProyectoData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Persona...");
                Collection<PersonaTabla> ArregloPersonas = new ArrayList<PersonaTabla> ();
		prograproyecto.Persona[] prograproyectoPersonas = prograproyecto.PersonaDAO.listPersonaByQuery(null, null);
                int length = Math.min(prograproyectoPersonas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(prograproyectoPersonas[i]);
                        
                        ArregloPersonas.add(PersonaTabla.createPersona(
                                prograproyectoPersonas[i].getPrimerNombre(), 
                                prograproyectoPersonas[i].getSegundoNombre(),
                                prograproyectoPersonas[i].getApellidoPaterno(),
                                prograproyectoPersonas[i].getApellidoMaterno(),
                                prograproyectoPersonas[i].getEdad(),  
                                prograproyectoPersonas[i].getUsuario_idUsuario()));
                        
		}
		String xml = new XStream(new DomDriver()).toXML(ArregloPersonas);
                System.out.println(xml);//
		
//		System.out.println("Listing Pais...");
//		prograproyecto.Pais[] prograproyectoPaises = prograproyecto.PaisDAO.listPaisByQuery(null, null);
//		length = Math.min(prograproyectoPaises.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoPaises[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Genero...");
//		prograproyecto.Genero[] prograproyectoGeneros = prograproyecto.GeneroDAO.listGeneroByQuery(null, null);
//		length = Math.min(prograproyectoGeneros.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoGeneros[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Usuario...");
//		prograproyecto.Usuario[] prograproyectoUsuarios = prograproyecto.UsuarioDAO.listUsuarioByQuery(null, null);
//		length = Math.min(prograproyectoUsuarios.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoUsuarios[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Redsocial...");
//		prograproyecto.Redsocial[] prograproyectoRedsocials = prograproyecto.RedsocialDAO.listRedsocialByQuery(null, null);
//		length = Math.min(prograproyectoRedsocials.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoRedsocials[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Proyecto...");
//		prograproyecto.Proyecto[] prograproyectoProyectos = prograproyecto.ProyectoDAO.listProyectoByQuery(null, null);
//		length = Math.min(prograproyectoProyectos.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoProyectos[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tipoproyecto...");
//		prograproyecto.Tipoproyecto[] prograproyectoTipoproyectos = prograproyecto.TipoproyectoDAO.listTipoproyectoByQuery(null, null);
//		length = Math.min(prograproyectoTipoproyectos.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTipoproyectos[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Material...");
//		prograproyecto.Material[] prograproyectoMaterials = prograproyecto.MaterialDAO.listMaterialByQuery(null, null);
//		length = Math.min(prograproyectoMaterials.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoMaterials[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tiendamaterial...");
//		prograproyecto.Tiendamaterial[] prograproyectoTiendamaterials = prograproyecto.TiendamaterialDAO.listTiendamaterialByQuery(null, null);
//		length = Math.min(prograproyectoTiendamaterials.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTiendamaterials[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tiendausuario...");
//		prograproyecto.Tiendausuario[] prograproyectoTiendausuarios = prograproyecto.TiendausuarioDAO.listTiendausuarioByQuery(null, null);
//		length = Math.min(prograproyectoTiendausuarios.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTiendausuarios[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tutorial...");
//		prograproyecto.Tutorial[] prograproyectoTutorials = prograproyecto.TutorialDAO.listTutorialByQuery(null, null);
//		length = Math.min(prograproyectoTutorials.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTutorials[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tiponudo...");
//		prograproyecto.Tiponudo[] prograproyectoTiponudos = prograproyecto.TiponudoDAO.listTiponudoByQuery(null, null);
//		length = Math.min(prograproyectoTiponudos.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTiponudos[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tipomaterial...");
//		prograproyecto.Tipomaterial[] prograproyectoTipomaterials = prograproyecto.TipomaterialDAO.listTipomaterialByQuery(null, null);
//		length = Math.min(prograproyectoTipomaterials.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTipomaterials[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tipohilo...");
//		prograproyecto.Tipohilo[] prograproyectoTipohilos = prograproyecto.TipohiloDAO.listTipohiloByQuery(null, null);
//		length = Math.min(prograproyectoTipohilos.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTipohilos[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Tipopatron...");
//		prograproyecto.Tipopatron[] prograproyectoTipopatrons = prograproyecto.TipopatronDAO.listTipopatronByQuery(null, null);
//		length = Math.min(prograproyectoTipopatrons.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoTipopatrons[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
//		
//		System.out.println("Listing Producto...");
//		prograproyecto.Producto[] prograproyectoProductos = prograproyecto.ProductoDAO.listProductoByQuery(null, null);
//		length = Math.min(prograproyectoProductos.length, ROW_COUNT);
//		for (int i = 0; i < length; i++) {
//			System.out.println(prograproyectoProductos[i]);
//		}
//		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public static void main(String[] args) {
		try {
			ListPrograProyectoData listPrograProyectoData = new ListPrograProyectoData();
			try {
				listPrograProyectoData.listTestData();
			}
			finally {
				prograproyecto.PrograProyectoPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
