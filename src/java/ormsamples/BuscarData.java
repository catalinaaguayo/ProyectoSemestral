/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormsamples;

import org.orm.PersistentException;
import prograproyecto.Persona;
import prograproyecto.Usuario;

/**
 *
 * @author catalinaaguayo
 */
public class BuscarData {

    /**
     * Busca el usuario por su nombre de usuario
     * @param nombreUsuario usuario a buscar
     * @return usuario encontrado o null en caso de que no lo encuentre
     * @throws PersistentException 
     */
    public static Usuario searchUsuario(String nombreUsuario) throws PersistentException {
        String query = "usuario.usuario='" + nombreUsuario + "'";
        try {
            return prograproyecto.UsuarioDAO.loadUsuarioByQuery(query, null);
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Buscar a la persona por su primer nombre
     * @param nombreBuscado primer nombre de la persona a buscar
     * @return persona encontrado o null en caso de que no lo encuentre
     */
    public Persona searchPersona(String nombreBuscado) {
        String query = "persona.primerNombre='" + nombreBuscado + "'";
        try {
            return prograproyecto.PersonaDAO.loadPersonaByQuery(query, null);
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }

}
