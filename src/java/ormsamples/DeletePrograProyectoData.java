/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeletePrograProyectoData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = prograproyecto.PrograProyectoPersistentManager.instance().getSession().beginTransaction();
		try {
			prograproyecto.Persona lprograproyectoPersona = prograproyecto.PersonaDAO.loadPersonaByQuery(null, null);
			// Delete the persistent object
			prograproyecto.PersonaDAO.delete(lprograproyectoPersona);
			prograproyecto.Pais lprograproyectoPais = prograproyecto.PaisDAO.loadPaisByQuery(null, null);
			// Delete the persistent object
			prograproyecto.PaisDAO.delete(lprograproyectoPais);
			prograproyecto.Genero lprograproyectoGenero = prograproyecto.GeneroDAO.loadGeneroByQuery(null, null);
			// Delete the persistent object
			prograproyecto.GeneroDAO.delete(lprograproyectoGenero);
			prograproyecto.Usuario lprograproyectoUsuario = prograproyecto.UsuarioDAO.loadUsuarioByQuery(null, null);
			// Delete the persistent object
			prograproyecto.UsuarioDAO.delete(lprograproyectoUsuario);
			prograproyecto.Redsocial lprograproyectoRedsocial = prograproyecto.RedsocialDAO.loadRedsocialByQuery(null, null);
			// Delete the persistent object
			prograproyecto.RedsocialDAO.delete(lprograproyectoRedsocial);
			prograproyecto.Proyecto lprograproyectoProyecto = prograproyecto.ProyectoDAO.loadProyectoByQuery(null, null);
			// Delete the persistent object
			prograproyecto.ProyectoDAO.delete(lprograproyectoProyecto);
			prograproyecto.Tipoproyecto lprograproyectoTipoproyecto = prograproyecto.TipoproyectoDAO.loadTipoproyectoByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TipoproyectoDAO.delete(lprograproyectoTipoproyecto);
			prograproyecto.Material lprograproyectoMaterial = prograproyecto.MaterialDAO.loadMaterialByQuery(null, null);
			// Delete the persistent object
			prograproyecto.MaterialDAO.delete(lprograproyectoMaterial);
			prograproyecto.Tiendamaterial lprograproyectoTiendamaterial = prograproyecto.TiendamaterialDAO.loadTiendamaterialByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TiendamaterialDAO.delete(lprograproyectoTiendamaterial);
			prograproyecto.Tiendausuario lprograproyectoTiendausuario = prograproyecto.TiendausuarioDAO.loadTiendausuarioByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TiendausuarioDAO.delete(lprograproyectoTiendausuario);
			prograproyecto.Tutorial lprograproyectoTutorial = prograproyecto.TutorialDAO.loadTutorialByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TutorialDAO.delete(lprograproyectoTutorial);
			prograproyecto.Tiponudo lprograproyectoTiponudo = prograproyecto.TiponudoDAO.loadTiponudoByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TiponudoDAO.delete(lprograproyectoTiponudo);
			prograproyecto.Tipomaterial lprograproyectoTipomaterial = prograproyecto.TipomaterialDAO.loadTipomaterialByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TipomaterialDAO.delete(lprograproyectoTipomaterial);
			prograproyecto.Tipohilo lprograproyectoTipohilo = prograproyecto.TipohiloDAO.loadTipohiloByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TipohiloDAO.delete(lprograproyectoTipohilo);
			prograproyecto.Tipopatron lprograproyectoTipopatron = prograproyecto.TipopatronDAO.loadTipopatronByQuery(null, null);
			// Delete the persistent object
			prograproyecto.TipopatronDAO.delete(lprograproyectoTipopatron);
			prograproyecto.Producto lprograproyectoProducto = prograproyecto.ProductoDAO.loadProductoByQuery(null, null);
			// Delete the persistent object
			prograproyecto.ProductoDAO.delete(lprograproyectoProducto);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeletePrograProyectoData deletePrograProyectoData = new DeletePrograProyectoData();
			try {
				deletePrograProyectoData.deleteTestData();
			}
			finally {
				prograproyecto.PrograProyectoPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
