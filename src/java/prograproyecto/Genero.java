/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Genero {
	public Genero() {
	}
	
	private int idGenero;
	
	private String genero;
	
	private prograproyecto.Persona persona;
	
	private void setIdGenero(int value) {
		this.idGenero = value;
	}
	
	public int getIdGenero() {
		return idGenero;
	}
	
	public int getORMID() {
		return getIdGenero();
	}
	
	public void setGenero(String value) {
		this.genero = value;
	}
	
	public String getGenero() {
		return genero;
	}
	
	public void setPersona(prograproyecto.Persona value) {
		this.persona = value;
	}
	
	public prograproyecto.Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdGenero());
	}
	
}
