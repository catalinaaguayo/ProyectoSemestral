/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Redsocial {
	public Redsocial() {
	}
	
	private int idRedSocial;
	
	private String usuarioFacebook;
	
	private String usuarioYoutube;
	
	private String usuarioInstagram;
	
	private String usuarioTwitter;
	
	private prograproyecto.Persona persona;
	
	private void setIdRedSocial(int value) {
		this.idRedSocial = value;
	}
	
	public int getIdRedSocial() {
		return idRedSocial;
	}
	
	public int getORMID() {
		return getIdRedSocial();
	}
	
	public void setUsuarioFacebook(String value) {
		this.usuarioFacebook = value;
	}
	
	public String getUsuarioFacebook() {
		return usuarioFacebook;
	}
	
	public void setUsuarioYoutube(String value) {
		this.usuarioYoutube = value;
	}
	
	public String getUsuarioYoutube() {
		return usuarioYoutube;
	}
	
	public void setUsuarioInstagram(String value) {
		this.usuarioInstagram = value;
	}
	
	public String getUsuarioInstagram() {
		return usuarioInstagram;
	}
	
	public void setUsuarioTwitter(String value) {
		this.usuarioTwitter = value;
	}
	
	public String getUsuarioTwitter() {
		return usuarioTwitter;
	}
	
	public void setPersona(prograproyecto.Persona value) {
		this.persona = value;
	}
	
	public prograproyecto.Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdRedSocial());
	}
	
}
