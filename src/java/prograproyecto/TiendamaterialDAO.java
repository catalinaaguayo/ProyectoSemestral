/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TiendamaterialDAO {
	public static Tiendamaterial loadTiendamaterialByORMID(int idTiendaMaterial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiendamaterialByORMID(session, idTiendaMaterial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial getTiendamaterialByORMID(int idTiendaMaterial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTiendamaterialByORMID(session, idTiendaMaterial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByORMID(int idTiendaMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiendamaterialByORMID(session, idTiendaMaterial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial getTiendamaterialByORMID(int idTiendaMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTiendamaterialByORMID(session, idTiendaMaterial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByORMID(PersistentSession session, int idTiendaMaterial) throws PersistentException {
		try {
			return (Tiendamaterial) session.load(prograproyecto.Tiendamaterial.class, new Integer(idTiendaMaterial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial getTiendamaterialByORMID(PersistentSession session, int idTiendaMaterial) throws PersistentException {
		try {
			return (Tiendamaterial) session.get(prograproyecto.Tiendamaterial.class, new Integer(idTiendaMaterial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByORMID(PersistentSession session, int idTiendaMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tiendamaterial) session.load(prograproyecto.Tiendamaterial.class, new Integer(idTiendaMaterial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial getTiendamaterialByORMID(PersistentSession session, int idTiendaMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tiendamaterial) session.get(prograproyecto.Tiendamaterial.class, new Integer(idTiendaMaterial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiendamaterial(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTiendamaterial(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiendamaterial(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTiendamaterial(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial[] listTiendamaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTiendamaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial[] listTiendamaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTiendamaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiendamaterial(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiendamaterial as Tiendamaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiendamaterial(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiendamaterial as Tiendamaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tiendamaterial", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial[] listTiendamaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTiendamaterial(session, condition, orderBy);
			return (Tiendamaterial[]) list.toArray(new Tiendamaterial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial[] listTiendamaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTiendamaterial(session, condition, orderBy, lockMode);
			return (Tiendamaterial[]) list.toArray(new Tiendamaterial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiendamaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiendamaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial loadTiendamaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tiendamaterial[] tiendamaterials = listTiendamaterialByQuery(session, condition, orderBy);
		if (tiendamaterials != null && tiendamaterials.length > 0)
			return tiendamaterials[0];
		else
			return null;
	}
	
	public static Tiendamaterial loadTiendamaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tiendamaterial[] tiendamaterials = listTiendamaterialByQuery(session, condition, orderBy, lockMode);
		if (tiendamaterials != null && tiendamaterials.length > 0)
			return tiendamaterials[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTiendamaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTiendamaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiendamaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTiendamaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiendamaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiendamaterial as Tiendamaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiendamaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiendamaterial as Tiendamaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tiendamaterial", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiendamaterial createTiendamaterial() {
		return new prograproyecto.Tiendamaterial();
	}
	
	public static boolean save(prograproyecto.Tiendamaterial tiendamaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().saveObject(tiendamaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(prograproyecto.Tiendamaterial tiendamaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().deleteObject(tiendamaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tiendamaterial tiendamaterial)throws PersistentException {
		try {
			if (tiendamaterial.getPais_idPais() != null) {
				tiendamaterial.getPais_idPais().getTiendaMaterial().remove(tiendamaterial);
			}
			
			prograproyecto.Material[] lMaterials = (prograproyecto.Material[])tiendamaterial.getMaterial().toArray(new prograproyecto.Material[tiendamaterial.getMaterial().size()]);
			for(int i = 0; i < lMaterials.length; i++) {
				lMaterials[i].setTiendaMaterial_idTiendaMaterial(null);
			}
			return delete(tiendamaterial);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tiendamaterial tiendamaterial, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (tiendamaterial.getPais_idPais() != null) {
				tiendamaterial.getPais_idPais().getTiendaMaterial().remove(tiendamaterial);
			}
			
			prograproyecto.Material[] lMaterials = (prograproyecto.Material[])tiendamaterial.getMaterial().toArray(new prograproyecto.Material[tiendamaterial.getMaterial().size()]);
			for(int i = 0; i < lMaterials.length; i++) {
				lMaterials[i].setTiendaMaterial_idTiendaMaterial(null);
			}
			try {
				session.delete(tiendamaterial);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(prograproyecto.Tiendamaterial tiendamaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().refresh(tiendamaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(prograproyecto.Tiendamaterial tiendamaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().evict(tiendamaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
