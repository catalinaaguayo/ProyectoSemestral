/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tutorial {
	public Tutorial() {
	}
	
	private int idTutorial;
	
	private Integer nombreTutorial;
	
	private Integer descripcionTutorial;
	
	private prograproyecto.Proyecto proyectoidProyecto;
	
	private void setIdTutorial(int value) {
		this.idTutorial = value;
	}
	
	public int getIdTutorial() {
		return idTutorial;
	}
	
	public int getORMID() {
		return getIdTutorial();
	}
	
	public void setNombreTutorial(int value) {
		setNombreTutorial(new Integer(value));
	}
	
	public void setNombreTutorial(Integer value) {
		this.nombreTutorial = value;
	}
	
	public Integer getNombreTutorial() {
		return nombreTutorial;
	}
	
	public void setDescripcionTutorial(int value) {
		setDescripcionTutorial(new Integer(value));
	}
	
	public void setDescripcionTutorial(Integer value) {
		this.descripcionTutorial = value;
	}
	
	public Integer getDescripcionTutorial() {
		return descripcionTutorial;
	}
	
	public void setProyectoidProyecto(prograproyecto.Proyecto value) {
		this.proyectoidProyecto = value;
	}
	
	public prograproyecto.Proyecto getProyectoidProyecto() {
		return proyectoidProyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdTutorial());
	}
	
}
