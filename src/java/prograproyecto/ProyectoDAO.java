/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class ProyectoDAO {
	public static Proyecto loadProyectoByORMID(int idProyecto) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadProyectoByORMID(session, idProyecto);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto getProyectoByORMID(int idProyecto) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getProyectoByORMID(session, idProyecto);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByORMID(int idProyecto, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadProyectoByORMID(session, idProyecto, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto getProyectoByORMID(int idProyecto, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getProyectoByORMID(session, idProyecto, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByORMID(PersistentSession session, int idProyecto) throws PersistentException {
		try {
			return (Proyecto) session.load(prograproyecto.Proyecto.class, new Integer(idProyecto));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto getProyectoByORMID(PersistentSession session, int idProyecto) throws PersistentException {
		try {
			return (Proyecto) session.get(prograproyecto.Proyecto.class, new Integer(idProyecto));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByORMID(PersistentSession session, int idProyecto, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Proyecto) session.load(prograproyecto.Proyecto.class, new Integer(idProyecto), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto getProyectoByORMID(PersistentSession session, int idProyecto, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Proyecto) session.get(prograproyecto.Proyecto.class, new Integer(idProyecto), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProyecto(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryProyecto(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProyecto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryProyecto(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto[] listProyectoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listProyectoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto[] listProyectoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listProyectoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProyecto(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Proyecto as Proyecto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProyecto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Proyecto as Proyecto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Proyecto", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto[] listProyectoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryProyecto(session, condition, orderBy);
			return (Proyecto[]) list.toArray(new Proyecto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto[] listProyectoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryProyecto(session, condition, orderBy, lockMode);
			return (Proyecto[]) list.toArray(new Proyecto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadProyectoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadProyectoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto loadProyectoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Proyecto[] proyectos = listProyectoByQuery(session, condition, orderBy);
		if (proyectos != null && proyectos.length > 0)
			return proyectos[0];
		else
			return null;
	}
	
	public static Proyecto loadProyectoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Proyecto[] proyectos = listProyectoByQuery(session, condition, orderBy, lockMode);
		if (proyectos != null && proyectos.length > 0)
			return proyectos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateProyectoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateProyectoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProyectoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateProyectoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProyectoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Proyecto as Proyecto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProyectoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Proyecto as Proyecto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Proyecto", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Proyecto createProyecto() {
		return new prograproyecto.Proyecto();
	}
	
	public static boolean save(prograproyecto.Proyecto proyecto) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().saveObject(proyecto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(prograproyecto.Proyecto proyecto) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().deleteObject(proyecto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Proyecto proyecto)throws PersistentException {
		try {
			if (proyecto.getUsuario_idUsuario() != null) {
				proyecto.getUsuario_idUsuario().getProyecto().remove(proyecto);
			}
			
			if (proyecto.getTipoProyecto_idTipo() != null) {
				proyecto.getTipoProyecto_idTipo().setProyecto(null);
			}
			
			if (proyecto.getProducto_idProducto() != null) {
				proyecto.getProducto_idProducto().setProyecto(null);
			}
			
			prograproyecto.Tipopatron[] lTipoPatrons = (prograproyecto.Tipopatron[])proyecto.getTipoPatron().toArray(new prograproyecto.Tipopatron[proyecto.getTipoPatron().size()]);
			for(int i = 0; i < lTipoPatrons.length; i++) {
				lTipoPatrons[i].setProyectoidProyecto(null);
			}
			prograproyecto.Tutorial[] lTutorials = (prograproyecto.Tutorial[])proyecto.getTutorial().toArray(new prograproyecto.Tutorial[proyecto.getTutorial().size()]);
			for(int i = 0; i < lTutorials.length; i++) {
				lTutorials[i].setProyectoidProyecto(null);
			}
			prograproyecto.Material[] lMaterials = (prograproyecto.Material[])proyecto.getMaterial().toArray(new prograproyecto.Material[proyecto.getMaterial().size()]);
			for(int i = 0; i < lMaterials.length; i++) {
				lMaterials[i].setProyectoidProyecto(null);
			}
			prograproyecto.Tiponudo[] lTipoNudos = (prograproyecto.Tiponudo[])proyecto.getTipoNudo().toArray(new prograproyecto.Tiponudo[proyecto.getTipoNudo().size()]);
			for(int i = 0; i < lTipoNudos.length; i++) {
				lTipoNudos[i].setProyectoidProyecto(null);
			}
			return delete(proyecto);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Proyecto proyecto, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (proyecto.getUsuario_idUsuario() != null) {
				proyecto.getUsuario_idUsuario().getProyecto().remove(proyecto);
			}
			
			if (proyecto.getTipoProyecto_idTipo() != null) {
				proyecto.getTipoProyecto_idTipo().setProyecto(null);
			}
			
			if (proyecto.getProducto_idProducto() != null) {
				proyecto.getProducto_idProducto().setProyecto(null);
			}
			
			prograproyecto.Tipopatron[] lTipoPatrons = (prograproyecto.Tipopatron[])proyecto.getTipoPatron().toArray(new prograproyecto.Tipopatron[proyecto.getTipoPatron().size()]);
			for(int i = 0; i < lTipoPatrons.length; i++) {
				lTipoPatrons[i].setProyectoidProyecto(null);
			}
			prograproyecto.Tutorial[] lTutorials = (prograproyecto.Tutorial[])proyecto.getTutorial().toArray(new prograproyecto.Tutorial[proyecto.getTutorial().size()]);
			for(int i = 0; i < lTutorials.length; i++) {
				lTutorials[i].setProyectoidProyecto(null);
			}
			prograproyecto.Material[] lMaterials = (prograproyecto.Material[])proyecto.getMaterial().toArray(new prograproyecto.Material[proyecto.getMaterial().size()]);
			for(int i = 0; i < lMaterials.length; i++) {
				lMaterials[i].setProyectoidProyecto(null);
			}
			prograproyecto.Tiponudo[] lTipoNudos = (prograproyecto.Tiponudo[])proyecto.getTipoNudo().toArray(new prograproyecto.Tiponudo[proyecto.getTipoNudo().size()]);
			for(int i = 0; i < lTipoNudos.length; i++) {
				lTipoNudos[i].setProyectoidProyecto(null);
			}
			try {
				session.delete(proyecto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(prograproyecto.Proyecto proyecto) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().refresh(proyecto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(prograproyecto.Proyecto proyecto) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().evict(proyecto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
