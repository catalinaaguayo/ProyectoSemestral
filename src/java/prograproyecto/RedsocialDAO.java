/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class RedsocialDAO {
	public static Redsocial loadRedsocialByORMID(int idRedSocial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadRedsocialByORMID(session, idRedSocial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial getRedsocialByORMID(int idRedSocial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getRedsocialByORMID(session, idRedSocial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByORMID(int idRedSocial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadRedsocialByORMID(session, idRedSocial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial getRedsocialByORMID(int idRedSocial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getRedsocialByORMID(session, idRedSocial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByORMID(PersistentSession session, int idRedSocial) throws PersistentException {
		try {
			return (Redsocial) session.load(prograproyecto.Redsocial.class, new Integer(idRedSocial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial getRedsocialByORMID(PersistentSession session, int idRedSocial) throws PersistentException {
		try {
			return (Redsocial) session.get(prograproyecto.Redsocial.class, new Integer(idRedSocial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByORMID(PersistentSession session, int idRedSocial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Redsocial) session.load(prograproyecto.Redsocial.class, new Integer(idRedSocial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial getRedsocialByORMID(PersistentSession session, int idRedSocial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Redsocial) session.get(prograproyecto.Redsocial.class, new Integer(idRedSocial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRedsocial(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryRedsocial(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRedsocial(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryRedsocial(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial[] listRedsocialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listRedsocialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial[] listRedsocialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listRedsocialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRedsocial(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Redsocial as Redsocial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRedsocial(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Redsocial as Redsocial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Redsocial", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial[] listRedsocialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRedsocial(session, condition, orderBy);
			return (Redsocial[]) list.toArray(new Redsocial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial[] listRedsocialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRedsocial(session, condition, orderBy, lockMode);
			return (Redsocial[]) list.toArray(new Redsocial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadRedsocialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadRedsocialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial loadRedsocialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Redsocial[] redsocials = listRedsocialByQuery(session, condition, orderBy);
		if (redsocials != null && redsocials.length > 0)
			return redsocials[0];
		else
			return null;
	}
	
	public static Redsocial loadRedsocialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Redsocial[] redsocials = listRedsocialByQuery(session, condition, orderBy, lockMode);
		if (redsocials != null && redsocials.length > 0)
			return redsocials[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRedsocialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateRedsocialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRedsocialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateRedsocialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRedsocialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Redsocial as Redsocial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRedsocialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Redsocial as Redsocial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Redsocial", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Redsocial createRedsocial() {
		return new prograproyecto.Redsocial();
	}
	
	public static boolean save(prograproyecto.Redsocial redsocial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().saveObject(redsocial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(prograproyecto.Redsocial redsocial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().deleteObject(redsocial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Redsocial redsocial)throws PersistentException {
		try {
			if (redsocial.getPersona() != null) {
				redsocial.getPersona().setRedSocial_idRedSocial(null);
			}
			
			return delete(redsocial);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Redsocial redsocial, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (redsocial.getPersona() != null) {
				redsocial.getPersona().setRedSocial_idRedSocial(null);
			}
			
			try {
				session.delete(redsocial);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(prograproyecto.Redsocial redsocial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().refresh(redsocial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(prograproyecto.Redsocial redsocial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().evict(redsocial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
