/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tipohilo {
	public Tipohilo() {
	}
	
	private int idTipoHilo;
	
	private String tipoHilo;
	
	private prograproyecto.Material material;
	
	private void setIdTipoHilo(int value) {
		this.idTipoHilo = value;
	}
	
	public int getIdTipoHilo() {
		return idTipoHilo;
	}
	
	public int getORMID() {
		return getIdTipoHilo();
	}
	
	public void setTipoHilo(String value) {
		this.tipoHilo = value;
	}
	
	public String getTipoHilo() {
		return tipoHilo;
	}
	
	public void setMaterial(prograproyecto.Material value) {
		this.material = value;
	}
	
	public prograproyecto.Material getMaterial() {
		return material;
	}
	
	public String toString() {
		return String.valueOf(getIdTipoHilo());
	}
	
}
