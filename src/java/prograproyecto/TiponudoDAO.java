/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TiponudoDAO {
	public static Tiponudo loadTiponudoByORMID(int idTipoNudo) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiponudoByORMID(session, idTipoNudo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo getTiponudoByORMID(int idTipoNudo) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTiponudoByORMID(session, idTipoNudo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByORMID(int idTipoNudo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiponudoByORMID(session, idTipoNudo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo getTiponudoByORMID(int idTipoNudo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTiponudoByORMID(session, idTipoNudo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByORMID(PersistentSession session, int idTipoNudo) throws PersistentException {
		try {
			return (Tiponudo) session.load(prograproyecto.Tiponudo.class, new Integer(idTipoNudo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo getTiponudoByORMID(PersistentSession session, int idTipoNudo) throws PersistentException {
		try {
			return (Tiponudo) session.get(prograproyecto.Tiponudo.class, new Integer(idTipoNudo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByORMID(PersistentSession session, int idTipoNudo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tiponudo) session.load(prograproyecto.Tiponudo.class, new Integer(idTipoNudo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo getTiponudoByORMID(PersistentSession session, int idTipoNudo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tiponudo) session.get(prograproyecto.Tiponudo.class, new Integer(idTipoNudo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiponudo(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTiponudo(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiponudo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTiponudo(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo[] listTiponudoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTiponudoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo[] listTiponudoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTiponudoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiponudo(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiponudo as Tiponudo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTiponudo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiponudo as Tiponudo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tiponudo", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo[] listTiponudoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTiponudo(session, condition, orderBy);
			return (Tiponudo[]) list.toArray(new Tiponudo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo[] listTiponudoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTiponudo(session, condition, orderBy, lockMode);
			return (Tiponudo[]) list.toArray(new Tiponudo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiponudoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTiponudoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo loadTiponudoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tiponudo[] tiponudos = listTiponudoByQuery(session, condition, orderBy);
		if (tiponudos != null && tiponudos.length > 0)
			return tiponudos[0];
		else
			return null;
	}
	
	public static Tiponudo loadTiponudoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tiponudo[] tiponudos = listTiponudoByQuery(session, condition, orderBy, lockMode);
		if (tiponudos != null && tiponudos.length > 0)
			return tiponudos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTiponudoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTiponudoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiponudoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTiponudoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiponudoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiponudo as Tiponudo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTiponudoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tiponudo as Tiponudo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tiponudo", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tiponudo createTiponudo() {
		return new prograproyecto.Tiponudo();
	}
	
	public static boolean save(prograproyecto.Tiponudo tiponudo) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().saveObject(tiponudo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(prograproyecto.Tiponudo tiponudo) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().deleteObject(tiponudo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tiponudo tiponudo)throws PersistentException {
		try {
			if (tiponudo.getProyectoidProyecto() != null) {
				tiponudo.getProyectoidProyecto().getTipoNudo().remove(tiponudo);
			}
			
			return delete(tiponudo);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tiponudo tiponudo, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (tiponudo.getProyectoidProyecto() != null) {
				tiponudo.getProyectoidProyecto().getTipoNudo().remove(tiponudo);
			}
			
			try {
				session.delete(tiponudo);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(prograproyecto.Tiponudo tiponudo) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().refresh(tiponudo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(prograproyecto.Tiponudo tiponudo) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().evict(tiponudo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
