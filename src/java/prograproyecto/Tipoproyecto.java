/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tipoproyecto {
	public Tipoproyecto() {
	}
	
	private int idTipo;
	
	private String tipo;
	
	private prograproyecto.Proyecto proyecto;
	
	private void setIdTipo(int value) {
		this.idTipo = value;
	}
	
	public int getIdTipo() {
		return idTipo;
	}
	
	public int getORMID() {
		return getIdTipo();
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setProyecto(prograproyecto.Proyecto value) {
		this.proyecto = value;
	}
	
	public prograproyecto.Proyecto getProyecto() {
		return proyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdTipo());
	}
	
}
