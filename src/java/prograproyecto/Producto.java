/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Producto {
	public Producto() {
	}
	
	private int idProducto;
	
	private int proyectoidProyecto;
	
	private String nombreProducto;
	
	private String descripcionProducto;
	
	private Integer precio;
	
	private prograproyecto.Proyecto proyecto;
	
	private void setIdProducto(int value) {
		this.idProducto = value;
	}
	
	public int getIdProducto() {
		return idProducto;
	}
	
	public int getORMID() {
		return getIdProducto();
	}
	
	public void setProyectoidProyecto(int value) {
		this.proyectoidProyecto = value;
	}
	
	public int getProyectoidProyecto() {
		return proyectoidProyecto;
	}
	
	public void setNombreProducto(String value) {
		this.nombreProducto = value;
	}
	
	public String getNombreProducto() {
		return nombreProducto;
	}
	
	public void setDescripcionProducto(String value) {
		this.descripcionProducto = value;
	}
	
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	
	public void setPrecio(int value) {
		setPrecio(new Integer(value));
	}
	
	public void setPrecio(Integer value) {
		this.precio = value;
	}
	
	public Integer getPrecio() {
		return precio;
	}
	
	public void setProyecto(prograproyecto.Proyecto value) {
		this.proyecto = value;
	}
	
	public prograproyecto.Proyecto getProyecto() {
		return proyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdProducto());
	}
	
}
