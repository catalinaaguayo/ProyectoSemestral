/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Material {
	public Material() {
	}
	
	private int idMaterial;
	
	private String nombreMaterial;
	
	private String descripcionMaterial;
	
	private prograproyecto.Tiendamaterial tiendaMaterial_idTiendaMaterial;
	
	private prograproyecto.Tipomaterial tipoMaterialidTipoMaterial;
	
	private prograproyecto.Tipohilo tipoHiloidTipoHilo;
	
	private prograproyecto.Proyecto proyectoidProyecto;
	
	private void setIdMaterial(int value) {
		this.idMaterial = value;
	}
	
	public int getIdMaterial() {
		return idMaterial;
	}
	
	public int getORMID() {
		return getIdMaterial();
	}
	
	public void setNombreMaterial(String value) {
		this.nombreMaterial = value;
	}
	
	public String getNombreMaterial() {
		return nombreMaterial;
	}
	
	public void setDescripcionMaterial(String value) {
		this.descripcionMaterial = value;
	}
	
	public String getDescripcionMaterial() {
		return descripcionMaterial;
	}
	
	public void setTiendaMaterial_idTiendaMaterial(prograproyecto.Tiendamaterial value) {
		this.tiendaMaterial_idTiendaMaterial = value;
	}
	
	public prograproyecto.Tiendamaterial getTiendaMaterial_idTiendaMaterial() {
		return tiendaMaterial_idTiendaMaterial;
	}
	
	public void setTipoMaterialidTipoMaterial(prograproyecto.Tipomaterial value) {
		this.tipoMaterialidTipoMaterial = value;
	}
	
	public prograproyecto.Tipomaterial getTipoMaterialidTipoMaterial() {
		return tipoMaterialidTipoMaterial;
	}
	
	public void setTipoHiloidTipoHilo(prograproyecto.Tipohilo value) {
		this.tipoHiloidTipoHilo = value;
	}
	
	public prograproyecto.Tipohilo getTipoHiloidTipoHilo() {
		return tipoHiloidTipoHilo;
	}
	
	public void setProyectoidProyecto(prograproyecto.Proyecto value) {
		this.proyectoidProyecto = value;
	}
	
	public prograproyecto.Proyecto getProyectoidProyecto() {
		return proyectoidProyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdMaterial());
	}
	
}
