/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tiendamaterial {
	public Tiendamaterial() {
	}
	
	private int idTiendaMaterial;
	
	private String nombreTienda;
	
	private prograproyecto.Pais pais_idPais;
	
	private java.util.Set material = new java.util.HashSet();
	
	private void setIdTiendaMaterial(int value) {
		this.idTiendaMaterial = value;
	}
	
	public int getIdTiendaMaterial() {
		return idTiendaMaterial;
	}
	
	public int getORMID() {
		return getIdTiendaMaterial();
	}
	
	public void setNombreTienda(String value) {
		this.nombreTienda = value;
	}
	
	public String getNombreTienda() {
		return nombreTienda;
	}
	
	public void setPais_idPais(prograproyecto.Pais value) {
		this.pais_idPais = value;
	}
	
	public prograproyecto.Pais getPais_idPais() {
		return pais_idPais;
	}
	
	public void setMaterial(java.util.Set value) {
		this.material = value;
	}
	
	public java.util.Set getMaterial() {
		return material;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTiendaMaterial());
	}
	
}
