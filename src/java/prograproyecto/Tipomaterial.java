/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tipomaterial {
	public Tipomaterial() {
	}
	
	private int idTipoMaterial;
	
	private String tipoMaterial;
	
	private prograproyecto.Material material;
	
	private void setIdTipoMaterial(int value) {
		this.idTipoMaterial = value;
	}
	
	public int getIdTipoMaterial() {
		return idTipoMaterial;
	}
	
	public int getORMID() {
		return getIdTipoMaterial();
	}
	
	public void setTipoMaterial(String value) {
		this.tipoMaterial = value;
	}
	
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	
	public void setMaterial(prograproyecto.Material value) {
		this.material = value;
	}
	
	public prograproyecto.Material getMaterial() {
		return material;
	}
	
	public String toString() {
		return String.valueOf(getIdTipoMaterial());
	}
	
}
