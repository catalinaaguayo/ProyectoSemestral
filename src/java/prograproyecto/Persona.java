/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Persona {
	public Persona() {
	}
	
	private int idPersona;
	
	private String primerNombre;
	
	private String segundoNombre;
	
	private String apellidoMaterno;
	
	private String apellidoPaterno;
	
	private Integer edad;
	
	private String correo;
	
	private prograproyecto.Pais pais_idPais;
	
	private prograproyecto.Redsocial redSocial_idRedSocial;
	
	private prograproyecto.Genero genero_idGenero;
	
	private prograproyecto.Usuario usuario_idUsuario;
	
	private void setIdPersona(int value) {
		this.idPersona = value;
	}
	
	public int getIdPersona() {
		return idPersona;
	}
	
	public int getORMID() {
		return getIdPersona();
	}
	
	public void setPrimerNombre(String value) {
		this.primerNombre = value;
	}
	
	public String getPrimerNombre() {
		return primerNombre;
	}
	
	public void setSegundoNombre(String value) {
		this.segundoNombre = value;
	}
	
	public String getSegundoNombre() {
		return segundoNombre;
	}
	
	public void setApellidoMaterno(String value) {
		this.apellidoMaterno = value;
	}
	
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	
	public void setApellidoPaterno(String value) {
		this.apellidoPaterno = value;
	}
	
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	
	public void setEdad(int value) {
		setEdad(new Integer(value));
	}
	
	public void setEdad(Integer value) {
		this.edad = value;
	}
	
	public Integer getEdad() {
		return edad;
	}
	
	public void setCorreo(String value) {
		this.correo = value;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public void setPais_idPais(prograproyecto.Pais value) {
		this.pais_idPais = value;
	}
	
	public prograproyecto.Pais getPais_idPais() {
		return pais_idPais;
	}
	
	public void setRedSocial_idRedSocial(prograproyecto.Redsocial value) {
		this.redSocial_idRedSocial = value;
	}
	
	public prograproyecto.Redsocial getRedSocial_idRedSocial() {
		return redSocial_idRedSocial;
	}
	
	public void setGenero_idGenero(prograproyecto.Genero value) {
		this.genero_idGenero = value;
	}
	
	public prograproyecto.Genero getGenero_idGenero() {
		return genero_idGenero;
	}
	
	public void setUsuario_idUsuario(prograproyecto.Usuario value) {
		this.usuario_idUsuario = value;
	}
	
	public prograproyecto.Usuario getUsuario_idUsuario() {
		return usuario_idUsuario;
	}
	
	public String toString() {
		return String.valueOf(getIdPersona());
	}
    }   
    
	
    
