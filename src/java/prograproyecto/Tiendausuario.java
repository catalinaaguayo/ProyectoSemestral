/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tiendausuario {
	public Tiendausuario() {
	}
	
	private int idTiendaUsuario;
	
	private String nombreTienda;
	
	private String paisTienda;
	
	private prograproyecto.Usuario usuario_idUsuario;
	
	private void setIdTiendaUsuario(int value) {
		this.idTiendaUsuario = value;
	}
	
	public int getIdTiendaUsuario() {
		return idTiendaUsuario;
	}
	
	public int getORMID() {
		return getIdTiendaUsuario();
	}
	
	public void setNombreTienda(String value) {
		this.nombreTienda = value;
	}
	
	public String getNombreTienda() {
		return nombreTienda;
	}
	
	public void setPaisTienda(String value) {
		this.paisTienda = value;
	}
	
	public String getPaisTienda() {
		return paisTienda;
	}
	
	public void setUsuario_idUsuario(prograproyecto.Usuario value) {
		this.usuario_idUsuario = value;
	}
	
	public prograproyecto.Usuario getUsuario_idUsuario() {
		return usuario_idUsuario;
	}
	
	public String toString() {
		return String.valueOf(getIdTiendaUsuario());
	}
	
}
