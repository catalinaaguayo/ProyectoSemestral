/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Proyecto {
	public Proyecto() {
	}
	
	private int idProyecto;
	
	private String nombre;
	
	private String descripcion;
	
	private prograproyecto.Usuario usuario_idUsuario;
	
	private prograproyecto.Tipoproyecto tipoProyecto_idTipo;
	
	private prograproyecto.Producto producto_idProducto;
	
	private java.util.Set tipoPatron = new java.util.HashSet();
	
	private java.util.Set tutorial = new java.util.HashSet();
	
	private java.util.Set material = new java.util.HashSet();
	
	private java.util.Set tipoNudo = new java.util.HashSet();
	
	private void setIdProyecto(int value) {
		this.idProyecto = value;
	}
	
	public int getIdProyecto() {
		return idProyecto;
	}
	
	public int getORMID() {
		return getIdProyecto();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setUsuario_idUsuario(prograproyecto.Usuario value) {
		this.usuario_idUsuario = value;
	}
	
	public prograproyecto.Usuario getUsuario_idUsuario() {
		return usuario_idUsuario;
	}
	
	public void setTipoProyecto_idTipo(prograproyecto.Tipoproyecto value) {
		this.tipoProyecto_idTipo = value;
	}
	
	public prograproyecto.Tipoproyecto getTipoProyecto_idTipo() {
		return tipoProyecto_idTipo;
	}
	
	public void setProducto_idProducto(prograproyecto.Producto value) {
		this.producto_idProducto = value;
	}
	
	public prograproyecto.Producto getProducto_idProducto() {
		return producto_idProducto;
	}
	
	public void setTipoPatron(java.util.Set value) {
		this.tipoPatron = value;
	}
	
	public java.util.Set getTipoPatron() {
		return tipoPatron;
	}
	
	
	public void setTutorial(java.util.Set value) {
		this.tutorial = value;
	}
	
	public java.util.Set getTutorial() {
		return tutorial;
	}
	
	
	public void setMaterial(java.util.Set value) {
		this.material = value;
	}
	
	public java.util.Set getMaterial() {
		return material;
	}
	
	
	public void setTipoNudo(java.util.Set value) {
		this.tipoNudo = value;
	}
	
	public java.util.Set getTipoNudo() {
		return tipoNudo;
	}
	
	
	public String toString() {
		return String.valueOf(getIdProyecto());
	}
	
}
