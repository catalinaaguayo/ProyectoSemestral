/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Pais {
	public Pais() {
	}
	
	private int idPais;
	
	private String pais;
	
	private String ciudad;
	
	private prograproyecto.Persona persona;
	
	private java.util.Set tiendaMaterial = new java.util.HashSet();
	
	private void setIdPais(int value) {
		this.idPais = value;
	}
	
	public int getIdPais() {
		return idPais;
	}
	
	public int getORMID() {
		return getIdPais();
	}
	
	public void setPais(String value) {
		this.pais = value;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setCiudad(String value) {
		this.ciudad = value;
	}
	
	public String getCiudad() {
		return ciudad;
	}
	
	public void setPersona(prograproyecto.Persona value) {
		this.persona = value;
	}
	
	public prograproyecto.Persona getPersona() {
		return persona;
	}
	
	public void setTiendaMaterial(java.util.Set value) {
		this.tiendaMaterial = value;
	}
	
	public java.util.Set getTiendaMaterial() {
		return tiendaMaterial;
	}
	
	
	public String toString() {
		return String.valueOf(getIdPais());
	}
	
}
