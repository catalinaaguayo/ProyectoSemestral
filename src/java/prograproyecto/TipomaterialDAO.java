/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TipomaterialDAO {
	public static Tipomaterial loadTipomaterialByORMID(int idTipoMaterial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTipomaterialByORMID(session, idTipoMaterial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial getTipomaterialByORMID(int idTipoMaterial) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTipomaterialByORMID(session, idTipoMaterial);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByORMID(int idTipoMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTipomaterialByORMID(session, idTipoMaterial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial getTipomaterialByORMID(int idTipoMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return getTipomaterialByORMID(session, idTipoMaterial, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByORMID(PersistentSession session, int idTipoMaterial) throws PersistentException {
		try {
			return (Tipomaterial) session.load(prograproyecto.Tipomaterial.class, new Integer(idTipoMaterial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial getTipomaterialByORMID(PersistentSession session, int idTipoMaterial) throws PersistentException {
		try {
			return (Tipomaterial) session.get(prograproyecto.Tipomaterial.class, new Integer(idTipoMaterial));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByORMID(PersistentSession session, int idTipoMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tipomaterial) session.load(prograproyecto.Tipomaterial.class, new Integer(idTipoMaterial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial getTipomaterialByORMID(PersistentSession session, int idTipoMaterial, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tipomaterial) session.get(prograproyecto.Tipomaterial.class, new Integer(idTipoMaterial), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipomaterial(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTipomaterial(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipomaterial(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return queryTipomaterial(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial[] listTipomaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTipomaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial[] listTipomaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return listTipomaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipomaterial(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tipomaterial as Tipomaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipomaterial(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tipomaterial as Tipomaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tipomaterial", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial[] listTipomaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTipomaterial(session, condition, orderBy);
			return (Tipomaterial[]) list.toArray(new Tipomaterial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial[] listTipomaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTipomaterial(session, condition, orderBy, lockMode);
			return (Tipomaterial[]) list.toArray(new Tipomaterial[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTipomaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return loadTipomaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial loadTipomaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tipomaterial[] tipomaterials = listTipomaterialByQuery(session, condition, orderBy);
		if (tipomaterials != null && tipomaterials.length > 0)
			return tipomaterials[0];
		else
			return null;
	}
	
	public static Tipomaterial loadTipomaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tipomaterial[] tipomaterials = listTipomaterialByQuery(session, condition, orderBy, lockMode);
		if (tipomaterials != null && tipomaterials.length > 0)
			return tipomaterials[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTipomaterialByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTipomaterialByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipomaterialByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = prograproyecto.PrograProyectoPersistentManager.instance().getSession();
			return iterateTipomaterialByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipomaterialByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tipomaterial as Tipomaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipomaterialByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From prograproyecto.Tipomaterial as Tipomaterial");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tipomaterial", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipomaterial createTipomaterial() {
		return new prograproyecto.Tipomaterial();
	}
	
	public static boolean save(prograproyecto.Tipomaterial tipomaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().saveObject(tipomaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(prograproyecto.Tipomaterial tipomaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().deleteObject(tipomaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tipomaterial tipomaterial)throws PersistentException {
		try {
			if (tipomaterial.getMaterial() != null) {
				tipomaterial.getMaterial().setTipoMaterialidTipoMaterial(null);
			}
			
			return delete(tipomaterial);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(prograproyecto.Tipomaterial tipomaterial, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (tipomaterial.getMaterial() != null) {
				tipomaterial.getMaterial().setTipoMaterialidTipoMaterial(null);
			}
			
			try {
				session.delete(tipomaterial);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(prograproyecto.Tipomaterial tipomaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().refresh(tipomaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(prograproyecto.Tipomaterial tipomaterial) throws PersistentException {
		try {
			prograproyecto.PrograProyectoPersistentManager.instance().getSession().evict(tipomaterial);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
