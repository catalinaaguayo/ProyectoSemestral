/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tiponudo {
	public Tiponudo() {
	}
	
	private int idTipoNudo;
	
	private String nudo;
	
	private prograproyecto.Proyecto proyectoidProyecto;
	
	private void setIdTipoNudo(int value) {
		this.idTipoNudo = value;
	}
	
	public int getIdTipoNudo() {
		return idTipoNudo;
	}
	
	public int getORMID() {
		return getIdTipoNudo();
	}
	
	public void setNudo(String value) {
		this.nudo = value;
	}
	
	public String getNudo() {
		return nudo;
	}
	
	public void setProyectoidProyecto(prograproyecto.Proyecto value) {
		this.proyectoidProyecto = value;
	}
	
	public prograproyecto.Proyecto getProyectoidProyecto() {
		return proyectoidProyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdTipoNudo());
	}
	
}
