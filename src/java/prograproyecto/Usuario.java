/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Usuario {
	public Usuario() {
	}
	
	private int idUsuario;
	
	private String usuario;
	
	private String contrasena;
	
	private java.util.Set proyecto = new java.util.HashSet();
	
	private prograproyecto.Tiendausuario tiendausuario;
	
	private prograproyecto.Persona persona;
	
	private void setIdUsuario(int value) {
		this.idUsuario = value;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public int getORMID() {
		return getIdUsuario();
	}
	
	public void setUsuario(String value) {
		this.usuario = value;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setContrasena(String value) {
		this.contrasena = value;
	}
	
	public String getContrasena() {
		return contrasena;
	}
	
	public void setProyecto(java.util.Set value) {
		this.proyecto = value;
	}
	
	public java.util.Set getProyecto() {
		return proyecto;
	}
	
	
	public void setTiendausuario(prograproyecto.Tiendausuario value) {
		this.tiendausuario = value;
	}
	
	public prograproyecto.Tiendausuario getTiendausuario() {
		return tiendausuario;
	}
	
	public void setPersona(prograproyecto.Persona value) {
		this.persona = value;
	}
	
	public prograproyecto.Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdUsuario());
	}
	
}
