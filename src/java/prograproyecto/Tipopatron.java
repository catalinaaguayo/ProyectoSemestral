/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package prograproyecto;

public class Tipopatron {
	public Tipopatron() {
	}
	
	private int idTipoPatron;
	
	private String tipoPatron;
	
	private prograproyecto.Proyecto proyectoidProyecto;
	
	private void setIdTipoPatron(int value) {
		this.idTipoPatron = value;
	}
	
	public int getIdTipoPatron() {
		return idTipoPatron;
	}
	
	public int getORMID() {
		return getIdTipoPatron();
	}
	
	public void setTipoPatron(String value) {
		this.tipoPatron = value;
	}
	
	public String getTipoPatron() {
		return tipoPatron;
	}
	
	public void setProyectoidProyecto(prograproyecto.Proyecto value) {
		this.proyectoidProyecto = value;
	}
	
	public prograproyecto.Proyecto getProyectoidProyecto() {
		return proyectoidProyecto;
	}
	
	public String toString() {
		return String.valueOf(getIdTipoPatron());
	}
	
}
