/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.orm.PersistentException;
import ormsamples.CrearData;

/**
 * Obtiene parámetros del formulario de registro, crea persona y usuario.
 * @author catalinaaguayo
 */
@WebServlet(name = "Registro", urlPatterns = {"/Registro"})
public class Registro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
        String primernombre = request.getParameter("primernombre");
        String segundonombre = request.getParameter("segundonombre");
        String apellidopaterno = request.getParameter("apellidopaterno");
        String apellidomaterno = request.getParameter("apellidomaterno");
        int edad = Integer.parseInt(request.getParameter("edad"));
        String correo = request.getParameter("correo");
        String genero = request.getParameter("genero");
        String pais = request.getParameter("pais");
        String facebook = request.getParameter("facebook");
        String youtube = request.getParameter("instagram");
        String instagram = request.getParameter("instagram");
        String twitter = request.getParameter("twitter");

        CrearData cd = new CrearData();
        try {
            cd.createTestData(primernombre, segundonombre, apellidomaterno, 
                    apellidopaterno, edad, correo, genero, pais, facebook, 
                    youtube, instagram, twitter);
        } catch (PersistentException ex) {
            System.out.println("error");
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("primernombre", primernombre);
        RequestDispatcher dispatcher = request.getRequestDispatcher("registroExitoso.jsp");
        dispatcher.forward(request, response);
        
        } catch (Exception e){
            System.out.println("error"+e.getMessage());
        } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
