/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import tablas.PersonaTabla;

/**
 * 
 * @author catalinaaguayo
 */
public class Persona {
    private static final int ROW_COUNT = 100;
	/**
         * Busca objeto de paciente y entrega un json o xml como salida
         * @param formato Indica si el resultado se entrega en XMl o JSON
         * @return 
         */
	static public String listTestData(String formato) {
		
		String query = null;//"persona.apellido_paterno = 'Mariqueo' ";
		Collection<PersonaTabla> ArregloPersonas = new ArrayList<PersonaTabla> ();
                prograproyecto.Persona[] prograproyectoPersonas;
                try {
                prograproyectoPersonas = prograproyecto.PersonaDAO.listPersonaByQuery(null, null);
		int length = Math.min(prograproyectoPersonas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
                        ArregloPersonas.add(PersonaTabla.createPersona(
                                prograproyectoPersonas[i].getPrimerNombre(), 
                                prograproyectoPersonas[i].getSegundoNombre(),
                                prograproyectoPersonas[i].getApellidoPaterno(),
                                prograproyectoPersonas[i].getApellidoMaterno(),
                                prograproyectoPersonas[i].getEdad(),  
                                prograproyectoPersonas[i].getUsuario_idUsuario())); 
		}
		} catch (PersistentException ex) {
                Logger.getLogger(PersonaTabla.class.getName()).log(Level.SEVERE, null, ex);
                }
                if("XML".equals(formato)){
                    XStream xstream = new XStream();
                    xstream.alias("paciente", PersonaTabla.class);
                    String xml = xstream.toXML(ArregloPersonas);
                     return xml;
                }
                else{
                    if ("JSON".equals(formato)) {
                        String json = new Gson().toJson(ArregloPersonas);
                        return json;
                    } else {
                        return "";
                    }
                
                }         
	}

}
