<%-- 
    Document   : contacto
    Created on : 08-nov-2017, 10:04:47
    Author     : catalinaaguayo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="ContactoEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contáctanos</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="index.jsp">Inicio</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
            <li style="float:right"><a class="usuario">${usuarioactual}</a></li>
        </ul>
    <center>
        <h2>Comunidad de</h2>
        <h1>macramé</h1>
        <p>Queremos ayudarte siempre</p>
        <p>Te responderemos tan rápido como podamos</p><br>
        <form action="Contacto" method="POST">
            <p>Nombre</p>
            <input type="text" name="nombre"/>
            <p>Correo</p>
            <input type="email" name="correo"/>
            <p>Cuéntanos en que te podemos ayudar o mejorar</p><br>
            <%--<input type="text" name="descripcion" style="width: 400px; height: 50px"/>--%>
            <textarea name="mensaje" rows="10" cols="60"></textarea>

            <br><br>
            <button type="submit" class="btn btn-secondary btnEstandar">Enviar</button>
        </form>
    </center>
</body>
</html>
