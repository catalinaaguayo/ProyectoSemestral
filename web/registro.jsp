<%-- 
    Document   : registro
    Created on : 18-oct-2017, 9:04:42
    Author     : catalinaaguayo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="RegistroEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrarse</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="index.jsp">Inicio</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
        </ul>
        <%-- Contenido --%>
        <form name="registrarse" action="Registro" method="POST">
            <center>
                <h2>Comunidad de</h2>
                <h1>macramé</h1>
                <p style="font-size: 20px;">Registro</p>
                <table border="0">
                    <thead>
                        <tr>
                            <th><p>DATOS PERSONALES</p></th>
                            <th><p></p></th>
                            <th><p>REDES SOCIALES</p></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>Primer nombre</p>
                                <input type="text" name="primernombre"/>
                            </td>
                            <td>                
                                <p>Segundo nombre</p>
                                <input type="text" name="segundonombre"/>
                            </td>
                            <td>
                                <div>
                                    <p>Facebook</p>
                                    <input type="text" name="facebook"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>                
                                <p>Apellido paterno</p>
                                <input type="text" name="apellidopaterno"/>
                            </td>
                            <td>                
                                <p>Apellido materno</p>
                                <input type="text" name="apellidomaterno"/>
                            </td>
                            <td>                
                                <p>Youtube</p>
                                <input type="text" name="youtube"/>
                            </td>
                        </tr>
                        <tr>
                            <td>                
                                <p>Edad</p>
                                <input type="number" name="edad"/>
                            </td>
                            <td>                
                                <p>Correo</p>
                                <input type="email" name="correo"/>
                            </td>
                            <td>                
                                <p>Instagram</p>
                                <input type="text" name="instagram"/>
                            </td>
                        </tr>
                        <tr>
                            <td>                
                                <p>Género</p> 
                                <select name="genero">
                                    <option>Femenino</option>
                                    <option>Masculino</option>
                                </select>
                            </td>
                            <td>                
                                <p>Pais</p>
                                <input type="text" name="pais"/>
                            </td>
                            <td>                
                                <p>Twitter</p>
                                <input type="text" name="twitter"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-secondary btnRegistro">Registrate</button>
            </center>        
        </form>
    </body>
</html>
