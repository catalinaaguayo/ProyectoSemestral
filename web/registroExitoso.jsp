<%-- 
    Document   : registroExitoso
    Created on : 18-oct-2017, 12:39:01
    Author     : catalinaaguayo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="RegistroExitosoEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro exitoso</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="index.jsp">Inicio</a></li>
            <li><a href="inicioSesion.jsp">Iniciar Sesion</a></li>
            <li><a href="aboutme.jsp">Sobre mi</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
        </ul>
        <%-- Contenido --%>
    <center>
        <p>¡Felicidades! </p>
        <p style="font-size: 25px">${primernombre}</p> 
        <p>ya eres parte de la</p>
        <h2>Comunidad de</h2>
        <h1>macramé</h1>
        <p style="font-size: 20px">Te hemos enviado tu nombre de usuario y contraseña a tu correo</p> 
        <a href="InicioSesion">
            <button type="button" class="btn btn-secondary btnInicio">Inicia Sesión</button>
        </a>
    </center>
</body>
</html>
