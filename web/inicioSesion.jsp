<%-- 
    Document   : inicioSesion
    Created on : 18-oct-2017, 11:03:40
    Author     : catalinaaguayo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="InicioSesionEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio de Sesión</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="index.jsp">Inicio</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
        </ul>
        <form name="iniciarSesion" action="InicioSesion" method="POST">
            <center>
                <h2>Comunidad de</h2>
                <h1>macramé</h1>
                <p style="font-size: 20px">Iniciar Sesion</p>
                <p>Usuario</p>
                <input type="text" name="usuario"/>
                <p>Contraseña</p>
                <input type="password" name="contrasena"/>
                <br>
                <p>${msg}</p>
                <br>
                <button type="submit" class="btn btn-secondary btnInicio">Iniciar Sesión</button>
            </center>
        </form>
    </body>
</html>
