<%-- 
    Document   : mensaje
    Created on : 17-oct-2017, 10:19:54
    Author     : catalinaaguayo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="InicioEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comunidad de macramé</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="inicio.jsp">Inicio</a></li>
            <li><a href="perfilUsuario.jsp">Mi perfil</a></li>
            <li><a href="creacionProyecto.jsp">Crear Proyecto</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
            <li style="float:right"><a class="usuario">${usuarioactual}</a></li>
        </ul>
        <%-- Contenido --%>
    <center>
        <div class="container">
            <p>Bienvenido/a</p>
            <p style="font-size: 20px">${usuario}</p>
            <h2>Comunidad de</h2>
            <h1>macramé</h1>
        </div>
    </center>
</body>
</html>
