<%-- 
    Document   : registro
    Created on : 18-oct-2017, 9:04:42
    Author     : catalinaaguayo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="CreacionProyectoEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crear proyecto</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="inicio.jsp">Inicio</a></li>
            <li><a href="perfilUsuario.jsp">Mi perfil</a></li>
            <li><a href="creacionProyecto.jsp">Crear Proyecto</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
            <li style="float:right"><a class="usuario">${usuarioactual}</a></li>
        </ul>
        <%-- Contenido --%>

            <center>
                <h2>Comunidad de</h2>
                <h1>macramé</h1>  
                <form action="CreacionExitosa" method="POST">               
                <p>Nombre del proyecto</p>
                <input type="text" name="nombreproyecto"/>
                <p>Descripción</p><br>
                <textarea name="descripcion" rows="10" cols="60"></textarea>
                <p>Tipo de proyecto</p>
                <select name="tipoproyecto">
                    <option value="pulsera">Pulsera (brazalete, manilla)</option>
                    <option value="aros">Aros (aretes, zarcillos)</option>
                    <option value="anillo">Anillo</option>
                    <option value="ropa">Ropa</option>
                    <option value="otros">Otros</option>
                </select>
                <p>Tipos de nudo (principal)</p>
                <select name="nudo">
                    <option value="nudobasico">Nudos básicos</option>
                    <option value="nudocordon">Nudos cordón (festón)</option>
                    <option value="nudoplano">Nudo plano (cuadrado)</option>
                </select>
                <p>Tipo de patrón</p>
                <select name="tipopatron">
                    <option value="flecha">Patron de flechas</option>
                    <option value="alpha">Patrón alpha</option>
                    <option value="espejo">Espejo</option>
                    <option value="indefinido">Indefinido</option>
                </select>

                <br>
                <button type="submit" class="btn btn-secondary btnEstandar">Crear</button>
            </center>
        </form>
    </body>
</html>
