<%-- 
    Document   : contacto
    Created on : 08-nov-2017, 10:04:47
    Author     : catalinaaguayo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="RedesSocialesEstilo.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redes Sociales</title>
    </head>
    <body>
        <%-- Navbar --%>
        <ul>
            <li style="float:left"><a class="active" href="redesSociales.jsp">Redes Sociales</a></li>
            <li><a href="index.jsp">Inicio</a></li>
            <li style="float:right"><a class="active" href="contacto.jsp">Contacto</a></li>
            <li style="float:right"><a class="usuario">${usuarioactual}</a></li>
        </ul>
    <center>
        <h2>Comunidad de</h2>
        <h1>macramé</h1>
        <p>Síguenos para no perderte nada</p>

        <table border="0" cellpadding="4px" cellspacing="4px">
            <thead>
                <tr>
                    <th><p>Youtube</p></th>
                    <th><p>Facebook</p</th>
                    <th><p>Instagram</p></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><div class="button" onclick="location.href = 'http://www.youtube.com/tutorialestco';"><label>F</label></div></td>
                    <td><div class="button" onclick="location.href = 'http://www.facebook.com/Tutorialestemuco';"><label>B</label></div></td>
                    <td><div class="button" onclick="location.href = 'http://www.instagram.com/cata_ag';"><label>V</label></div></td>
                </tr>
                <tr>
                    <td><p>Tutoriales Temuco</p></td>
                    <td><p>Tutoriales Temuco</p></td>
                    <td><p>@cata_ag</p></td>
                </tr>
            </tbody>
        </table>
    </center>
</body>
</html>
